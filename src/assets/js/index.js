import '../scss/app.scss'
import Hammer from 'hammerjs'

const timeline = document.querySelector('.timeline ol'),
  arrowPrev = document.querySelector('.arrow__prev'),
  arrowNext = document.querySelector('.arrow__next'),
  disabledClass = 'disabled'

function openInNewTab (url) {
  let win = window.open(url, '_blank')
  win.focus()
}

// function showContent (el, allNodes) {
//   allNodes.forEach(element => {
//     if (!element.classList.contains('hide') && element !== el) {
//       element.classList.add('hide')
//     } else if (element === el) {
//       element.classList.remove('hide')
//     }
//   })
// }

function init () {
  setSwipeFn(timeline, arrowPrev, arrowNext)
  setKeyboardFn(arrowPrev, arrowNext)
}

function setBtnState (el, flag = true) {
  if (flag) {
    el.classList.add(disabledClass)
  } else {
    if (el.classList.contains(disabledClass)) {
      el.classList.remove(disabledClass)
    }
    el.disabled = false
  }
}

function setSwipeFn (tl, prev, next) {
  const hammer = new Hammer(tl)
  hammer.on('swipeleft', () => next.click())
  hammer.on('swiperight', () => prev.click())
}

function setKeyboardFn (prev, next) {
  document.addEventListener('keydown', (e) => {
    if ((e.which === 37) || (e.which === 39)) {
      const timelineOfTop = timeline.offsetTop
      const y = window.pageYOffset
      if (timelineOfTop !== y) {
        window.scrollTo(0, timelineOfTop)
      }
      if (e.which === 37) {
        prev.click()
      } else if (e.which === 39) {
        next.click()
      }
    }
  })
}

window.addEventListener('load', init)

let eventEmitter = document.querySelectorAll('.timeline li')

if (eventEmitter !== undefined && eventEmitter !== null) {
  eventEmitter.forEach(el => {
    // el.addEventListener('mouseenter', e => {
    //   showContent(el, eventEmitter)
    // })

    el.addEventListener('click', e => {
      if (el.dataset.href !== undefined) {
        openInNewTab(el.dataset.href)
      } else {
        openInNewTab('#')
      }
    })
  })
}

if (arrowNext !== undefined && arrowNext !== null) {
  arrowNext.addEventListener('click', () => {
    let indexSelected = Array.prototype.slice.call(eventEmitter).findIndex(e => e.dataset.isselected === 'true')
    let width = 0
    for (let i = 0; i <= indexSelected; i++) {
      width += eventEmitter[i].offsetWidth
    }
    if (indexSelected < eventEmitter.length - 2) {
      eventEmitter[indexSelected].dataset.isselected = 'false'
      eventEmitter[indexSelected + 1].dataset.isselected = 'true'
    }
    if (indexSelected === eventEmitter.length - 3) {
      arrowNext.disabled = true
      setBtnState(arrowNext, true)
    }
    setBtnState(arrowPrev, false)
    timeline.style.transform = `translateX(-${width}px)`
  })
}

if (arrowPrev !== undefined && arrowPrev !== null) {
  arrowPrev.addEventListener('click', () => {
    let indexSelected = Array.prototype.slice.call(eventEmitter).findIndex(e => e.dataset.isselected === 'true')
    let width = 0
    if (eventEmitter[indexSelected - 1] !== undefined) {
      width = eventEmitter[indexSelected - 1].offsetWidth
    }
    const tlStyle = getComputedStyle(timeline)
    const tlTransform = tlStyle.getPropertyValue('-webkit-transform') || tlStyle.getPropertyValue('transform')
    const values = parseInt(tlTransform.split(',')[4]) + parseInt(`${width}`)
    if (indexSelected > 0) {
      eventEmitter[indexSelected].dataset.isselected = 'false'
      eventEmitter[indexSelected - 1].dataset.isselected = 'true'
    } else {
      arrowPrev.disabled = true
      setBtnState(arrowPrev, true)
    }
    setBtnState(arrowNext, false)
    timeline.style.transform = `translateX(${values}px)`
  })
}
